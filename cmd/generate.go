package cmd

import (
	"fmt"
	"image"
	"math/rand"
	"os"
	"strconv"
	"time"

	"../lib/fire"
	"../lib/hsl"
	"../lib/perlin"
	"../lib/rgb"
	"../lib/tools"

	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
)

var (
	out  string
	w    int
	h    int
	oct  int
	ps   int
	freq float32
	amp  float32
	per  float32
)

func init() {
	rootCmd.AddCommand(generateCmd)

	generateCmd.AddCommand(generateRGBCmd)
	generateCmd.AddCommand(generateHSLCmd)
	generateCmd.AddCommand(generateFireCmd)

	flag.StringVar(&out, "out", "perlin", "the name of the output file.")
	flag.IntVar(&w, "w", 500, "the width of the image to generate.")
	flag.IntVar(&h, "h", 500, "the height of the image to generate.")

	flag.IntVar(&oct, "oct", 4, "(>1) the number of octaves to generate.")
	flag.IntVar(&ps, "ps", 256, "the number of permutations to consider.")
	flag.Float32Var(&freq, "freq", 16, "the frequency to start with (doubles).")
	flag.Float32Var(&amp, "amp", 5, "the amplitude to start with.")
	flag.Float32Var(&per, "per", 0.75, "the amount to scale the amp each octave.")
}

var generateCmd = &cobra.Command{
	Use:   "generate",
	Short: "Commands relating to generating images.",
}

var generateRGBCmd = &cobra.Command{
	Use:   "rgb <r> <g> <b> ",
	Short: "Generate an image with rgb",
	Args:  cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		ival := []int{}
		for _, v := range args {
			iv, err := strconv.Atoi(v)
			if err != nil {
				fmt.Printf("error converting '%s' to int: %q\n", v, err)
				os.Exit(2)
			}
			if iv < 0 || iv > 255 {
				fmt.Printf("rgb values must be between 0 and 255 (inclusive): saw '%s'\n", iv)
				os.Exit(2)
			}
			ival = append(ival, iv)
		}
		generateImage(
			cmd,
			rgb.Painter{
				R: ival[0],
				B: ival[1],
				G: ival[2]})
	},
}

var generateHSLCmd = &cobra.Command{
	Use:   "hsl <shift>", // <h> <s> <l> <filename> <width> [height]",
	Short: "Generate an image with hsl values",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		s, err := strconv.ParseFloat(args[0], 64)
		if err != nil {
			fmt.Println("shift '%s' must be a valid float64: %q\n", args[0], err)
			os.Exit(2)
		}
		generateImage(cmd, hsl.Painter{Shift: s})
	},
}

var generateFireCmd = &cobra.Command{
	Use:   "fire",
	Short: "Generate a firey image",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		generateImage(cmd, fire.Painter{})
	},
}

type Painter interface {
	// v should be from 0.0 to 1.0
	PaintCoord(img *image.RGBA, x, y int, v float32)
}

func generateImage(cmd *cobra.Command, pa Painter) {
	rand.Seed(time.Now().UTC().UnixNano())

	p := perlin.NewPerlin(ps, pa)

	img := p.GetBlur(w, h, oct, freq, amp, per)
	err := tools.OutputImage(out, img.SubImage(image.Rect(0, 0, w, h)))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
