# Golang 2D Perlin Noise Generator.
Author: https://bitbucket.org/Im_Jooms/

## How to build

```shell
go build -o perlin main.go 
```

## Help menu

```
$ ./perlin help
Generate Perlin noise 2D images.

Usage:
  perlin [command]

Available Commands:
  generate    Commands relating to generating images.
  help        Help about any command

Flags:
      --amp float32    the amplitude to start with. (default 5)
      --freq float32   the frequency to start with (doubles). (default 16)
      --h int          the height of the image to generate. (default 500)
  -h, --help           help for perlin
      --oct int        (>1) the number of octaves to generate. (default 4)
      --out string     the name of the output file. (default "perlin")
      --per float32    the amount to scale the amp each octave. (default 0.75)
      --ps int         the number of permutations to consider. (default 256)
      --w int          the width of the image to generate. (default 500)

Use "perlin [command] --help" for more information about a command.

$ ./perlin generate help
Commands relating to generating images.

Usage:
  perlin generate [command]

Available Commands:
  fire        Generate a firey image
  hsl         Generate an image with hsl values
  rgb         Generate an image with rgb

Flags:
  -h, --help   help for generate

Global Flags:
      --amp float32    the amplitude to start with. (default 5)
      --freq float32   the frequency to start with (doubles). (default 16)
      --h int          the height of the image to generate. (default 500)
      --oct int        (>1) the number of octaves to generate. (default 4)
      --out string     the name of the output file. (default "perlin")
      --per float32    the amount to scale the amp each octave. (default 0.75)
      --ps int         the number of permutations to consider. (default 256)
      --w int          the width of the image to generate. (default 500)

Use "perlin generate [command] --help" for more information about a command.
```

## Example usage

```shell
 ./perlin generate fire --oct 16 --amp 4 --freq 3
```

## Example output
![Example Fire Image](perlin.png)
