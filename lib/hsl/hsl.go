// Package hsl provides an HSL Painter to paint with.
package hsl

import (
	"image"
	"image/color"
)

type Painter struct {
	Shift float64
}

func (p Painter) PaintCoord(img *image.RGBA, x, y int, v float32) {
	if v < 0 {
		v = 0
	} else if v > 1.0 {
		v = 1
	}

	// Hue goes from 0 to 360.
	// Saturation is always full
	// Lightness is aways 50%
	h := (float64(v) + p.Shift)
	s := 1.0
	l := 0.5

	r, g, b := HSLtoRGB(h, s, l)
	c := color.RGBA{r, g, b, 255}

	img.SetRGBA(x, y, c)
}

// From CSS3 spec
func HSLtoRGB(h, s, l float64) (uint8, uint8, uint8) {
	m2 := 0.0
	if l <= 0.5 {
		m2 = l * (s + 1)
	} else {
		m2 = l + s - l*s
	}
	m1 := l*2 - m2

	r := Hue2RGB(m1, m2, h+1.0/3)
	g := Hue2RGB(m1, m2, h)
	b := Hue2RGB(m1, m2, h-1.0/3)

	return uint8(r * 255), uint8(g * 255), uint8(b * 255)
}

// From CSS3 spec
func Hue2RGB(m1, m2, h float64) float64 {
	if h < 0 {
		h += 1
	}
	if h > 1 {
		h -= 1
	}
	if h*6 < 1 {
		return m1 + (m2-m1)*h*6
	}
	if h*2 < 1 {
		return m2
	}
	if h*3 < 2 {
		return m1 + (m2-m1)*(2.0/3.0-h)*6
	}
	return m1
}
