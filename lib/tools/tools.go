// Package tools provides general tools to image generation binaries.
package tools

import (
	"fmt"
	"image"
	"image/png"
	"os"
)

// OutputImage saves a given image to a png file.
func OutputImage(fileName string, i image.Image) error {
	out, err := os.Create(fmt.Sprintf("%s.png", fileName))
	if err != nil {
		return err
	}

	png.Encode(out, i)
	if err != nil {
		return err
	}

	return nil
}
