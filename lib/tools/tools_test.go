package tools

import (
	"testing"
)

func TestHSLtoRGB(t *testing.T) {

	cases := []struct {
		h float64
		s float64
		l float64
		r uint8
		g uint8
		b uint8
	}{
		{
			//White
			h: 0,
			s: 1,
			l: 1,
			r: 255,
			g: 255,
			b: 255,
		},
		{
			//Black
			h: 0,
			s: 1,
			l: 0,
			r: 0,
			g: 0,
			b: 0,
		},
		{
			//Red
			h: 0,
			s: 1,
			l: 0.5,
			r: 255,
			g: 0,
			b: 0,
		},
		{
			//Blue
			h: 2.0 / 3,
			s: 1,
			l: 0.5,
			r: 0,
			g: 0,
			b: 255,
		},
		{
			//Green
			h: 1.0 / 3,
			s: 1,
			l: 0.5,
			r: 0,
			g: 255,
			b: 0,
		},
		{
			//Grey
			h: 0,
			s: 0,
			l: 0.5,
			r: 127,
			g: 127,
			b: 127,
		},
		{
			//Olive
			h: 1.0 / 6,
			s: 1,
			l: 0.25,
			r: 127,
			g: 127,
			b: 0,
		},
		{
			//Orangish
			h: 48.0 / 360,
			s: 1,
			l: 0.5,
			r: 255,
			g: 204,
			b: 0,
		},
	}

	for _, c := range cases {
		r, g, b := HSLtoRGB(c.h, c.s, c.l)
		if r != c.r || g != c.g || b != c.b {
			t.Fatalf("HSLtoRGB(%f,%f,%f)= %d %d %d, want %d %d %d", c.h, c.s, c.l, r, g, b, c.r, c.g, c.b)
		}
	}
}
