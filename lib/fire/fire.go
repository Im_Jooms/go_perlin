// Package fire provides a Fire Painter to paint with.
package fire

import (
	"image"
	"image/color"
	"math"

	"../../lib/hsl"
)

type Painter struct{}

func (p Painter) PaintCoord(img *image.RGBA, x, y int, v float32) {
	if v < 0 {
		v = 0
	} else if v > 1.0 {
		v = 1
	}

	// Hue goes from 0 to 55 (red to yellow)
	// Saturation is always full
	// lightness is a relatively severe Sigmoid function
	h := float64(v) * (1.0 / 6)
	s := 1.0
	sev := 10.0
	newV := float64(v)*sev - sev/2
	l := (newV/math.Sqrt(1+math.Pow(newV, 2)))/2 + 0.5

	r, g, b := hsl.HSLtoRGB(h, s, l)
	c := color.RGBA{r, g, b, 255}

	img.SetRGBA(x, y, c)
}
