// Package rgb provides an RGB Painter to paint with.
package rgb

import (
	"image"
	"image/color"
)

type Painter struct {
	R int
	G int
	B int
}

func (p Painter) PaintCoord(img *image.RGBA, x, y int, v float32) {
	if v < 0 {
		v = 0
	} else if v > 1.0 {
		v = 1
	}

	pr := uint8(v * float32(p.R))
	pg := uint8(v * float32(p.G))
	pb := uint8(v * float32(p.B))

	c := color.RGBA{pr, pg, pb, 255}

	img.SetRGBA(x, y, c)
}
