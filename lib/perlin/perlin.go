// The perlin library defines tools needed to generate an image with perlin
// noise.
package perlin

import (
	"image"
	"math/rand"
)

type Painter interface {
	// v should be from 0.0 to 1.0
	PaintCoord(img *image.RGBA, x, y int, v float32)
}

type Perlin struct {
	permutation []int
	numPerms    int
	permSize    int
	paint       Painter
}

// NewPerlin creates and sets up a new Perlin instance with the default
// settings.
func NewPerlin(permutations int, pa Painter) *Perlin {
	p := &Perlin{
		numPerms: permutations,
	}
	p.generatePermutations()
	p.paint = pa
	return p
}

func (p *Perlin) SetPainter(pa Painter) {
	p.paint = pa
}

func (p *Perlin) generatePermutations() {
	// Create
	p.permSize = p.numPerms * 2
	p.permutation = make([]int, p.permSize)
	for i := 0; i < p.numPerms; i++ {
		p.permutation[i] = i
		p.permutation[i+p.numPerms] = i
	}

	// Shuffle -- Knuth
	for i := 0; i < p.numPerms-2; i++ {
		j := rand.Intn(p.numPerms - i)
		ii := i + p.numPerms
		// Swap
		p.permutation[i], p.permutation[i+j] = p.permutation[i+j], p.permutation[i]
		p.permutation[ii], p.permutation[ii+j] = p.permutation[ii+j], p.permutation[ii]
	}
}

func fade(t float32) float32 {
	return t * t * t * (t*(t*6-15) + 10)
}

func lerp(a, b, x float32) float32 {
	return a + x*(b-a)
}

// Pick a random vector
func grad(hash int, x, y float32) float32 {
	h := hash % 4
	switch h {
	case 0:
		return x + y
	case 1:
		return -x + y
	case 2:
		return x - y
	case 3:
		return -x - y
	default:
		return 0
	}
}

func (p *Perlin) inc(x int) int {
	x += 1
	if x > p.numPerms {
		x = p.numPerms
	}
	return x
}

func (p *Perlin) Coord2D(x, y float32) float32 {
	xi := int(x) % p.numPerms
	yi := int(y) % p.numPerms
	xf := x - float32(int(x))
	yf := y - float32(int(y))
	u := fade(xf)
	v := fade(yf)

	aa := p.permutation[p.permutation[xi]+yi]
	ab := p.permutation[p.permutation[xi]+p.inc(yi)]
	ba := p.permutation[p.permutation[p.inc(xi)]+yi]
	bb := p.permutation[p.permutation[p.inc(xi)]+p.inc(yi)]

	x1 := lerp(grad(aa, xf, yf),
		grad(ba, xf-1, yf),
		u)
	x2 := lerp(grad(ab, xf, yf-1),
		grad(bb, xf-1, yf-1),
		u)

	// Binding to 0 to 1 instead of -1 to 1
	return (lerp(x1, x2, v) + 1) / 2
}

func (p *Perlin) GetBlur(w, h, oct int, freq, amp, per float32) image.RGBA {
	img := image.NewRGBA(image.Rect(0, 0, w, h))
	for y := 0; y < h; y++ {
		py := float32(y) / float32(h)
		for x := 0; x < w; x++ {
			px := float32(x) / float32(w)
			v := p.octCoord2D(py, px, freq, amp, per, oct)
			p.paint.PaintCoord(img, x, y, v)
		}
	}
	return *img
}

func (p *Perlin) octCoord2D(x, y, freq, amp, per float32, oct int) float32 {
	var total float32
	var maxValue float32
	for i := 0; i < oct; i++ {
		total += p.Coord2D(x*freq, y*freq) * amp
		maxValue += amp
		amp *= per
		freq *= 2
	}
	return total / maxValue
}
